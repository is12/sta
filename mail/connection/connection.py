import imaplib
import smtplib
import ssl
from functools import partial
from typing import Callable, Dict, Iterable, Tuple, Union

from nonion import Maybe, Pipeline, next_, try_

from mail.message import Message, create_empty_message, parse_message

from .defaults import (DEFAULT_ADDRESS_SEPARATOR, DEFAULT_IMAP_PORT,
                       DEFAULT_MESSAGE_FORMAT, DEFAULT_SMTP_PORT)

INCLUDING = 1


def create_smtp_connection(address: str) -> Maybe[smtplib.SMTP]:
    ip, port = split_address(address)
    port, *_ = port or (DEFAULT_SMTP_PORT,)

    context = ssl.create_default_context()
    connection: Maybe[smtplib.SMTP] = try_(smtplib.SMTP_SSL)(ip, port, context=context)

    return connection


def split_address(x: str) -> Tuple[str, Maybe[int]]:
    ip, *port = x.split(DEFAULT_ADDRESS_SEPARATOR)

    port = map(int, port)
    port: Maybe[int] = next_(port)

    return ip, port


def sign_in(
    connection: Union[smtplib.SMTP, imaplib.IMAP4], credentials: Tuple[str, str]
) -> Maybe[Union[smtplib.SMTP, imaplib.IMAP4]]:
    is_credentials_accepted: Maybe = try_(connection.login)(*credentials)
    return is_credentials_accepted and (connection,)


def send_message(connection: smtplib.SMTP, message: Message) -> Message:
    send_status: Maybe[Dict[str, object]] = try_(connection.send_message)(message)

    if not send_status:
        return create_empty_message()

    send_error, *_ = send_status

    if send_error:
        return create_empty_message()

    return message


def create_imap_connection(address: str) -> Maybe[imaplib.IMAP4]:
    ip, port = split_address(address)
    port, *_ = port or (DEFAULT_IMAP_PORT,)

    context = ssl.create_default_context()
    connection: Maybe[imaplib.IMAP4_SSL] = try_(imaplib.IMAP4_SSL)(
        ip, port, ssl_context=context
    )

    return connection


def receive_messages(connection: imaplib.IMAP4) -> Iterable[Message]:
    count: Maybe[int] = get_messages_count(connection)

    if not count:
        return ()

    count, *_ = count

    identifiers = range(INCLUDING, count + INCLUDING)
    identifiers = reversed(identifiers)
    identifiers = map(str, identifiers)

    receive: Callable[[str], Message] = partial(receive_message, connection)
    return map(receive, identifiers)


@try_
def get_messages_count(connection: imaplib.IMAP4) -> int:
    _, count = connection.select()
    count, *_ = count

    count = count.decode()
    return int(count)


def receive_message(connection: imaplib.IMAP4, identifier: str) -> Message:
    raw_message: Maybe[bytes] = receive_raw_message(connection, identifier)
    message: Maybe[Message] = Pipeline(raw_message) * parse_message >> tuple

    message, *_ = message or (create_empty_message(),)
    return message


@try_
def receive_raw_message(connection: imaplib.IMAP4, identifier: str) -> bytes:
    _, raw_message = connection.fetch(identifier, DEFAULT_MESSAGE_FORMAT)

    raw_message, *_ = raw_message
    _, raw_message = raw_message

    return raw_message
