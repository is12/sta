import os
from email import message_from_bytes
from email.message import EmailMessage, Message
from email.utils import make_msgid
from io import IOBase
from typing import Dict, Iterable, Tuple, Union

from nonion import Maybe, Pipeline, fail, find, shift, try_

from utility.numeric import union_dictionaries

from .fields import (ATTACHMENTS_FIELD, CONTENT_FIELD, IDENTIFIER_FIELD,
                     NAME_FIELD, PATH_FIELD, PAYLOAD_FIELD)

ATTACHMENT_PARAMETERS = {"maintype": "application", "subtype": "octet-stream"}
EMPTY = ""


def load_message(raw: Dict[str, object]) -> Message:
    raw = raw.copy()
    x = EmailMessage()

    content: str = raw.pop(CONTENT_FIELD, EMPTY)
    try_(x.set_content)(content)

    raw_attachments: object = raw.pop(ATTACHMENTS_FIELD, ())

    if isinstance(raw_attachments, (list, tuple)):
        attachments: Iterable[Tuple[str, bytes]] = load_attachments(raw_attachments)
        x: Message = add_attachments(x, attachments)

    for k, v in raw.items():
        try_(x.__setitem__)(k, v)

    if IDENTIFIER_FIELD not in x:
        try_(x.__setitem__)(IDENTIFIER_FIELD, make_msgid())

    return x


def load_attachments(raw: Iterable[object]) -> Iterable[Tuple[str, bytes]]:
    return Pipeline(raw) % shift(isinstance, dict) * load_attachment


def load_attachment(raw: Dict[str, object]) -> Maybe[Tuple[str, bytes]]:
    path = raw.get(PATH_FIELD, EMPTY)
    content: Maybe[bytes] = load_content(path)

    name = raw.get(NAME_FIELD, EMPTY) or os.path.basename(path)
    name = str(name)

    return ((name, *content),) if content else ()


@fail(lambda _: ())
def load_content(path: str) -> Maybe[bytes]:
    with open(path, "rb") as h:
        return (h.read(),)


def add_attachments(x: Message, attachments: Iterable[Tuple[str, bytes]]) -> Message:
    for yz in attachments:
        x: Message = add_attachment(x, *yz)

    return x


def add_attachment(x: Message, name: str, content: bytes) -> Message:
    x.add_attachment(content, filename=name, **ATTACHMENT_PARAMETERS)
    return x


def dump_message_with_payload(x: Message) -> Dict[str, object]:
    dumped: Dict[str, object] = dump_message(x)
    payload: Maybe[Union[Tuple[Message, ...], str]] = try_(x.get_payload)()

    if payload:
        dumped[PAYLOAD_FIELD] = dump_payload(*payload)

    return dumped


def dump_message(x: Message) -> Dict[str, object]:
    return dict(x)


def dump_payload(
    raw: Union[Tuple[Message, ...], str]
) -> Union[Tuple[Dict[str, object], ...], str]:
    if not isinstance(raw, (list, tuple)):
        return str(raw)

    return (
        Pipeline(raw) % shift(isinstance, Message) / dump_message_with_payload >> tuple
    )


def create_empty_message() -> Message:
    x = EmailMessage()
    x[IDENTIFIER_FIELD] = EMPTY

    return x


def parse_message(raw: bytes) -> Maybe[Message]:
    return try_(message_from_bytes)(raw)


def union_with_submessage_type(message: Message, message_type: str) -> Dict[str, str]:
    submessages = message.walk()
    submessage: Maybe[Message] = find(lambda x: x.get_content_type() == message_type)(
        submessages
    )

    messages = *submessage, message
    return union_messages(messages)


def union_messages(xs: Iterable[Message]) -> Dict[str, str]:
    dumped: Iterable[Dict[str, str]] = map(dump_message_with_payload, xs)
    return union_dictionaries(dumped)
