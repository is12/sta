from argparse import ArgumentParser

from .defaults import DEFAULT_SERVER


def create_common_parser(instruction: str) -> ArgumentParser:
    parser = ArgumentParser(instruction)

    parser.add_argument(
        "-s",
        "--server",
        type=str,
        default=DEFAULT_SERVER,
        help="your email server in form of **address:port** or in form **address**",
    )
    parser.add_argument(
        "-l", "--login", type=str, default="", help="your email server account login"
    )
    parser.add_argument(
        "-P",
        "--password",
        type=str,
        default="",
        help="your email server account password; using this option is unsafe, use prompt instead",
    )

    return parser
