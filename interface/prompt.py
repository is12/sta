import sys
from getpass import getpass

from .defaults import DEFAULT_LOGIN_PROMPT, DEFAULT_PASSWORD_PROMPT

EMPTY = ""


def prompt_login(prompt: str = DEFAULT_LOGIN_PROMPT) -> str:
    print(prompt, end=EMPTY, file=sys.stderr)
    return input()


def prompt_password(prompt: str = DEFAULT_PASSWORD_PROMPT) -> str:
    return getpass(prompt)
