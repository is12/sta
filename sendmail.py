import json
import smtplib
import sys
from argparse import ArgumentParser
from functools import partial
from typing import Callable, Dict, Iterable, Tuple

from nonion import Maybe, Pipeline, try_

from interface.defaults import DEFAULT_INDENT
from interface.parser import create_common_parser
from interface.prompt import prompt_login, prompt_password
from mail.connection import create_smtp_connection, send_message, sign_in
from mail.message import (FROM_FIELD, Message, dump_message_with_payload,
                          load_message)
from utility.loader import load_json
from utility.numeric import apply_to_keys, put_if_missing


def create_parser() -> ArgumentParser:
    instruction = """
  Script reads messages from .json on stdin or from --path and sends them using smtp
  """
    parser: ArgumentParser = create_common_parser(instruction)

    parser.add_argument(
        "-p",
        "--path",
        type=str,
        default="-",
        help="path to .json containing messages to be send",
    )

    return parser


parser: ArgumentParser = create_parser()
args = parser.parse_args()

login: str = args.login or prompt_login()
password: str = args.password or prompt_password()

raw_messages: Maybe[Tuple[Dict[str, object], ...]] = load_json(args.path)

if not raw_messages:
    print("Unable to load messages", file=sys.stderr)
    sys.exit(1)

raw_messages: Iterable[Dict[str, object]] = Pipeline(*raw_messages) * try_(dict)

capitalize: Callable[[Dict[str, object]], Dict[str, object]] = partial(
    apply_to_keys, str.capitalize
)
raw_messages: Iterable[Dict[str, object]] = map(capitalize, raw_messages)

put_login: Callable[[Dict[str, object]], Dict[str, object]] = partial(
    put_if_missing, key=FROM_FIELD, value=login
)
raw_messages: Iterable[Dict[str, object]] = map(put_login, raw_messages)

messages: Iterable[Message] = map(load_message, raw_messages)

connection: Maybe[smtplib.SMTP] = create_smtp_connection(args.server)

if not connection:
    print(f"Unable to connect to {args.server}", file=sys.stderr)
    sys.exit(2)

connection, *_ = connection
connection: Maybe[smtplib.SMTP] = sign_in(connection, (login, password))

if not connection:
    print("Invalid login or password", file=sys.stderr)
    sys.exit(3)

connection, *_ = connection
send: Callable[[Message], Message] = partial(send_message, connection)

sent_messages: Iterable[Message] = map(send, messages)
sent_messages = tuple(sent_messages)

connection.close()

raw_sent_messages: Iterable[Dict[str, str]] = map(
    dump_message_with_payload, sent_messages
)
raw_sent_messages = tuple(raw_sent_messages)

print(json.dumps(raw_sent_messages, indent=DEFAULT_INDENT))
