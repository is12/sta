import imaplib
import json
import sys
from argparse import ArgumentParser
from functools import partial
from itertools import islice
from typing import Callable, Dict, Iterable, Tuple

from nonion import Maybe, Pipeline, finds, try_

from interface.defaults import (DEFAULT_INDENT, DEFAULT_LOCAL_FIELD,
                                DEFAULT_MAXIMUM_MESSAGES, DEFAULT_MESSAGE_TYPE,
                                DEFAULT_REMOTE_FIELD)
from interface.parser import create_common_parser
from interface.prompt import prompt_login, prompt_password
from mail.connection import create_imap_connection, receive_messages, sign_in
from mail.message import Message, union_with_submessage_type
from utility.loader import load_json

EMPTY = ""


def create_parser() -> ArgumentParser:
    instruction = """
  Script reads identifiers of messages from .json on stdin or from --path and receives those
  messages from server
  """
    parser: ArgumentParser = create_common_parser(instruction)

    parser.add_argument(
        "-p",
        "--path",
        type=str,
        default="-",
        help="path to .json containing identifiers of messages to be received",
    )
    parser.add_argument(
        "-lf",
        "--local-field",
        type=str,
        default=DEFAULT_LOCAL_FIELD,
        help="name of field containing identifier in single object in .json",
    )
    parser.add_argument(
        "-rf",
        "--remote-field",
        type=str,
        default=DEFAULT_REMOTE_FIELD,
        help="name of field containing identifier in single received message",
    )
    parser.add_argument(
        "-m",
        "--maximum-messages",
        type=int,
        default=DEFAULT_MAXIMUM_MESSAGES,
        help="maximum number of messages to receive from the server; by default read all messages",
    )
    parser.add_argument(
        "-t",
        "--type",
        type=str,
        default=DEFAULT_MESSAGE_TYPE,
        help="type of received message content",
    )

    return parser


def create_identifier_checker(field: str, value: str) -> Callable[[Message], bool]:
    def check(x: Message) -> bool:
        return (field in x) and (value in x[field])

    return check


def remove_option_level(x: Maybe[Dict[str, str]]) -> Dict[str, str]:
    if not x:
        return {}

    x, *_ = x
    return x


parser: ArgumentParser = create_parser()
args = parser.parse_args()

login: str = args.login or prompt_login()
password: str = args.password or prompt_password()

raw_identifiers: Maybe[Tuple[Dict[str, object], ...]] = load_json(args.path)

if not raw_identifiers:
    print("Unable to load identifiers of messages", file=sys.stderr)
    sys.exit(1)

raw_identifiers: Iterable[Dict[str, object]] = Pipeline(*raw_identifiers) * try_(dict)

identifiers = map(lambda x: x.get(args.local_field, EMPTY), raw_identifiers)
identifiers = map(str, identifiers)

connection: Maybe[imaplib.IMAP4] = create_imap_connection(args.server)

if not connection:
    print(f"Unable to connect to {args.server}", file=sys.stderr)
    sys.exit(2)

connection, *_ = connection
connection: Maybe[imaplib.IMAP4] = sign_in(connection, (login, password))

if not connection:
    print("Invalid login or password", file=sys.stderr)
    sys.exit(3)

connection, *_ = connection
messages: Iterable[Message] = receive_messages(connection)

if args.maximum_messages > 0:
    messages = islice(messages, args.maximum_messages)

create_checker: Callable[[str], Callable[[Message], bool]] = partial(
    create_identifier_checker, args.remote_field
)
checkers: Iterable[Callable[[Message], bool]] = map(create_checker, identifiers)

matched_messages: Iterable[Maybe[Message]] = finds(checkers)(messages)
matched_messages = tuple(matched_messages)

connection.close()

union: Callable[[Message], Dict[str, str]] = partial(
    union_with_submessage_type, message_type=args.type
)
matched_messages: Iterable[Iterable[Dict[str, str]]] = map(
    lambda x: map(union, x), matched_messages
)
matched_messages = map(tuple, matched_messages)

matched_messages: Iterable[Dict[str, str]] = map(remove_option_level, matched_messages)
matched_messages = tuple(matched_messages)

print(json.dumps(matched_messages, indent=DEFAULT_INDENT))
