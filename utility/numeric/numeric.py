from functools import reduce
from itertools import chain
from typing import Callable, Dict, Iterable, TypeVar

X = TypeVar("X")
Y = TypeVar("Y")
Z = TypeVar("Z")


def apply_to_keys(f: Callable[[X], Y], dictionary: Dict[X, Z]) -> Dict[Y, Z]:
    xs, zs = zip(*dictionary.items())
    ys = map(f, xs)

    applied = zip(ys, zs)
    return dict(applied)


def apply_to_values(f: Callable[[X], Y], dictionary: Dict[Z, X]) -> Dict[Z, Y]:
    zs, xs = zip(*dictionary.items())
    ys = map(f, xs)

    applied = zip(zs, ys)
    return dict(applied)


def put_if_missing(dictionary: Dict[X, Y], key: X, value: Y) -> Dict[X, Y]:
    if key in dictionary:
        return dictionary

    dictionary = dictionary.copy()
    dictionary[key] = value

    return dictionary


def union_dictionaries(xs: Iterable[Dict[object, object]]) -> Dict[object, object]:
    return reduce(binary_union_dictionaries, xs, {})


def binary_union_dictionaries(
    x: Dict[object, object], y: Dict[object, object]
) -> Dict[object, object]:
    union = {}

    for k, v in chain(x.items(), y.items()):
        union.setdefault(k, v)

    return union
