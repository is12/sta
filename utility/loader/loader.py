import json
from typing import Dict, Tuple

import click
from nonion import Maybe, fail


@fail(lambda _: ())
def load_json(x: str) -> Maybe[Tuple[Dict[str, object], ...]]:
    with click.open_file(x) as h:
        return (json.load(h),)
